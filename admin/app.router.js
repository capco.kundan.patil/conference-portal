app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider
    // HOME STATES
        .state('login', {
            url: '/login',
            templateUrl: './views/temp_login.html'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: './views/temp_dashboard.html'
        })
        .state('registration', {
            url: '/registration',
            templateUrl: './views/temp-registration.html'
        })
        .state('dashboard.default', {
            url: '/registration',
            templateUrl: './views/temp_dashboard_default.html'
        })
        .state('home.paragraph', {
            url: '/paragraph',
            template: 'I could sure use a drink right now.'
        })
        .state('about', {
            url: '/about',
            views: {
                '': {templateUrl: 'partial-about.html'},
                'columnOne@about': {template: 'Look I am a column!'},
                'columnTwo@about': {
                    templateUrl: 'table-data.html',
                    controller: 'scotchController'
                }
            }
        });
});